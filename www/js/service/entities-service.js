(function (angular) {
  'use strict';

  angular.module('starter')
    .factory('EntitiesService', entitiesService)
    .factory('NationalitiesService', nationalitiesService)
    .factory('UserService', userService)
    .factory('ImagesService', imagesService)
    .factory('LocalService', localService);

  entitiesService.$inject = ['$http', '$log'];
  nationalitiesService.$inject = ['$http', '$log'];
  userService.$inject = ['$http', '$log'];
  imagesService.$inject = ['$http', '$log'];
  localService.$inject = ['$localStorage', '$log'];


  function entitiesService($http, $log) {
    return {

      listEntity: function () {
        return $http.get('js/data/listEntity.json')
          .then(function (response) {
            /*          $log.info("-----------------------------------------");
                        $log.info("listEntity:");
                        $log.info(response);
                        $log.info("-----------------------------------------"); */
            return response.data.payload;
          });
      },

      saveEntity: function (entity) {
        return $http.get('js/data/saveEntity.json')
          .then(function (response) {
            /*          $log.info("-----------------------------------------");
                        $log.info("saveEntity:");
                        $log.info(response);
                        $log.info("-----------------------------------------"); */
            return response.data.payload;
          });
      },

      deleteEntity: function (entity) {
        return $http.get('js/data/deleteEntity.json')
          .then(function (response) {
            /*          $log.info("-----------------------------------------");
                        $log.info("deleteEntity:");
                        $log.info(response);
                        $log.info("-----------------------------------------");*/
            return response.data.payload;
          });
      },

      updateEntity: function (entity) {
        return $http.get('js/data/updateEntity.json')
          .then(function (response) {
            /*          $log.info("-----------------------------------------");
                        $log.info("updateEntity:");
                        $log.info(response);
                        $log.info("-----------------------------------------"); */
            return response.data.payload;
          });
      }

    }
  }

  function nationalitiesService($http, $log) {
    return {

      getNationalities: function () {
        return $http.get('js/data/nationalities.json').then(function (response) {
          /*        $log.info("-----------------------------------------");
                    $log.info("getCountries:");
                    $log.info(response);
                    $log.info("-----------------------------------------"); */
          return response;
        });
      }

    }
  }

  function userService($http, $log) {
    return {

      login: function (user) {
        return $http.get('js/data/login.json').then(function (response) {
          var user;
          /*        $log.info("-----------------------------------------");
                    $log.info("login:");
                    $log.info(response);
                    $log.info("-----------------------------------------"); */
          return response.data.payload[0];
        });
      }

    }
  }

  function imagesService($http, $log) {
    return {

      getImage: function (userId, id, imageId) {
        return $http.get('js/data/getImage.json')
          .then(function (response) {
            /*          $log.info("-----------------------------------------");
                        $log.info("getImage:");
                        $log.info(response.data.payload);
                        $log.info("-----------------------------------------"); */
            return response.data.payload[0];
          },
          function (response) {
            return response.data.msg;
          });
      },

      deleteImages: function (userId, id, fileName) {
        return $http.get('js/data/deleteImages.json')
          .then(function (response) {
            /*          $log.info("-----------------------------------------");
                        $log.info("deleteImages:");
                        $log.info(response.data.payload);
                        $log.info("-----------------------------------------"); */
            return response.data.payload[0];
          },
          function (response) {
            return response.data.msg;
          });
      },

      createImage: function (userId, id, fileName, serviceData) {
        return $http.get('js/data/createImage.json')
          .then(function (response) {
            /*        $log.info("-----------------------------------------");
                      $log.info("createImage:");
                      $log.info(response.data.payload);
                      $log.info("-----------------------------------------"); */
            return response.data.payload[0];
          },
          function (response) {
            return response.data.msg;
          });
      }

    }
  }

  function localService($localStorage, $log) {

    return {
      set: function (key, value) {
        return localStorage.setItem(key, JSON.stringify(value));
      },
      get: function (key) {
        return JSON.parse(localStorage.getItem(key));
      },
      destroy: function (key) {
        return localStorage.removeItem(key);
      }

    };
  }

})(angular);