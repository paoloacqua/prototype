'use strict';

(function (angular) {

  angular
    .module('Detail', [])
    .config(function ($stateProvider, $urlRouterProvider) {
      $stateProvider

      .state('app.detail', {
        url: '/detail',
        params: { obj: null },
        views: {
          'menuContent': {
            templateUrl: 'Detail/templates/entity-detail.html',
            controller: 'EntityDetailCtrl as Entity'
          }
        }
      })

    });

})(angular);


 