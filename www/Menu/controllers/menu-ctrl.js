(function (angular) {
  'use strict';

  angular.module('starter').controller('MenuCtrl', menuCtrl);

  menuCtrl.$inject = ['$scope', '$ionicModal', '$ionicPopup', '$ionicListDelegate', '$rootScope', 'LocalService'];

  function menuCtrl($scope, $ionicModal, $ionicPopup, $ionicListDelegate, $rootScope, LocalService) {

    $scope.userLoggedIn = {};

    $ionicModal.fromTemplateUrl('Login/templates/login.html', function (modal) {
      $scope.loginCtrl = modal;
    }, {
        scope: $scope,
        focusFirstInput: true
      });

    $scope.logging = function () {
      if ($scope.userLoggedIn.name != undefined) {
        $ionicPopup.confirm({
          title: 'Warning',
          template: '<p class="text-center">' + $scope.userLoggedIn.name.charAt(0).toUpperCase() + $scope.userLoggedIn.name.substr(1).toLowerCase() + ' would you logout?</p>',
          cancelType: 'button-assertive'
        }).then(function (res) {
          if (res) {
            $ionicListDelegate.closeOptionButtons();
            $rootScope.$broadcast('user-loggedOut');
          }
        });
      } else {
        $scope.loginCtrl.show();
      }
    };

    $scope.$on('user-loggedIn', function (event, args) {
      $scope.userLoggedIn = LocalService.get('user-loggedIn');
    });

    $scope.$on('user-loggedOut', function (event, args) {
      $scope.userLoggedIn = {};
      LocalService.destroy('user-loggedIn');
    });

  }

})(angular);