'use strict';

(function (angular) {

angular
    .module('Menu', [])
    .config(function ($stateProvider, $urlRouterProvider) {
      $stateProvider

        .state('app', {
          url: '/app',
          templateUrl: 'Menu/templates/menu.html',
          controller: 'MenuCtrl'
        });

    });

})(angular);

