'use strict';

(function (angular) {

  angular
    .module('Home', [])
    .config(function ($stateProvider, $urlRouterProvider) {
      $stateProvider

        .state('app.home', {
          url: '/home',
          views: {
            'menuContent': {
              templateUrl: 'Home/templates/home.html',
              controller: 'HomeCtrl as Entities'
            }
          }
        });

    });

})(angular);

