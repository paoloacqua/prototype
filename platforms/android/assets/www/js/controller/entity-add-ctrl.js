(function (angular) {
  'use strict';

  angular.module('starter').controller('EntityAddUpdCtrl', entityAddUpdCtrl);

  entityAddUpdCtrl.$inject = ['$scope', '$rootScope', '$ionicLoading', '$cordovaCamera', '$log', 'EntitiesService', 'NationalitiesService'];

  function entityAddUpdCtrl($scope, $rootScope, $ionicLoading, $cordovaCamera, $log, EntitiesService, NationalitiesService) {

    $scope.entity = {
    };
    $scope.nationalities = {};
    $scope.genders = [
      {
        "name": "Male",
        "code": "M"
      },
      {
        "name": "Female",
        "code": "F"
      },
      {
        "name": "Other",
        "code": "O"
      }
    ];


    $scope.entity.pictureBook = 'img/user.png';

    function modalShownEvent(evt, data) {
      if ($scope.selectedEntity.name) {
        NationalitiesService.getNationalities()
          .then(function (response) {
            $scope.nationalities = response.data.nationalities;
          });
        $scope.entity.name = $scope.selectedEntity.name;
        $scope.entity.surname = $scope.selectedEntity.surname;
        $scope.entity.nationality = $scope.selectedEntity.nationality;
        $scope.entity.gender = $scope.selectedEntity.gender;
        $scope.entity.description = $scope.selectedEntity.description;
        $scope.entity.pictureBook = $scope.selectedEntity.pictureBook;
        // WARNIG: INSERT EVEN "age" FIELD
      } else {
        $scope.entity.pictureBook = 'img/user.png';
      }
    }

    $scope.closeAddEntity = function () {
      $scope.entity.pictureBook = '';
      $scope.entity = {};
      $scope.homeCtrl.hide();
    };

    $scope.saveEntity = function () {
      $ionicLoading.show({ templateUrl: 'templates/spinner.html' });
      EntitiesService.saveEntity($scope.entity)
        .then(function () {
        }).finally(function () {
          $ionicLoading.hide();
          $scope.closeAddEntity();
        });
    }

    $scope.choosePhoto = function () {
      var options = {
        quality: 75,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.entity.pictureBook = "data:image/jpeg;base64," + imageData;
      }, function (err) {
        // An error occured. Show a message to the user
      });
    }

    $scope.$on('modal.shown', modalShownEvent);

  }
})(angular);

