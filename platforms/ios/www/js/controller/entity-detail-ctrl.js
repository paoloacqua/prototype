(function (angular) {
  'use strict';

  angular.module('starter').controller('EntityDetailCtrl', entityDetailCtrl);

  entityDetailCtrl.$inject = ['$scope', '$log', '$ionicActionSheet',
    '$stateParams', '$q', '$ionicPopup', '$ionicListDelegate',
    '$ionicLoading', '$cordovaCamera', '$rootScope', '$ionicModal',
    '$ionicSlideBoxDelegate', 'ImagesService', 'LocalService'];

  function entityDetailCtrl($scope, $log, $ionicActionSheet, $stateParams,
    $q, $ionicPopup, $ionicListDelegate, $ionicLoading,
    $cordovaCamera, $rootScope, $ionicModal, $ionicSlideBoxDelegate,
    ImagesService, LocalService) {

    var vm = this;

    vm.openTab1 = true;
    vm.openTab2 = false;
    vm.shouldShowDelete = false;
    vm.obj = {};
    vm.images = [];
    vm.userLoggedIn = {};
    vm.enableTab1 = enableTab1;
    vm.enableTab2 = enableTab2;
    vm.toDelete = toDelete;
    vm.showActionsheet = showActionsheet;
    $scope.photosToDelete = [];
    vm.closeModal = closeModal;
    vm.slideChanged = slideChanged;
    vm.goToSlide = goToSlide;

    $ionicModal.fromTemplateUrl('templates/slide-image.html', {
      scope: $scope,
    }).then(function (modal) {
      $scope.modal = modal;
    });

    function getListPhoto() {
      var callsList = [];
      var key = "";
      var id = "";
      var userId = "";
      for (var i = 0; i < vm.obj.albums.length; i++) {
        // id = vm.obj.id;
        userId = vm.obj.userId;
        key = Object.keys(vm.obj.albums[i])[0];
        for (var y = 0; y < vm.obj.albums[i][key].length; y++) {
          callsList.push(ImagesService.getImage(userId, id, vm.obj.albums[i][key][y]));
        }
      }
      $ionicLoading.show({ templateUrl: 'templates/spinner.html' });
      $q.all(callsList)
        .then(function (response) {
          for (var i = 0; i < response.length; i++) {
            /* Hack */ /* images[i].pictureBook = response[z].pictureBook; */
            vm.images.push({ filename: response[i].fileName, pictureBook: "https://s-media-cache-ak0.pinimg.com/736x/d9/88/06/d988063a29579bed950cc3e519bda2f4--cafe-racer-triumph-triumph-motorcycles.jpg" });
          }
        }).finally(function () {
          $ionicLoading.hide();
        });
    }

    function enableTab1() {
      vm.openTab2 = false;
      vm.openTab1 = true;
    };

    function enableTab2() {
      vm.openTab1 = false;
      vm.openTab2 = true;
    };

    function toDelete(fileName) {
      $scope.photosToDelete.push(fileName);
    }


    function showActionsheet() {
      $ionicActionSheet.show({
        buttons: [
          { text: '<i class="assertive icon ion-trash-b"></i> <div class="assertive">Delete photos</div>' },
          { text: '<i class="assertive icon ion-plus"></i> <div class="assertive">Add photo</div>' }
        ],
        buttonClicked: function (index) {
          vm.enableTab2();
          switch (index) {
            case 0:
              if (vm.images.length == 0) {
                $ionicPopup.alert({
                  title: 'Attention',
                  template: '<div class="text-center">There are no photos to delete</div>',
                  buttons: [{
                    text: 'OK',
                    type: 'button-assertive'
                  }]
                });
                return false;
              } else {
                if (vm.shouldShowDelete && $scope.photosToDelete.length > 0) {
                  $ionicPopup.confirm({
                    title: 'Warning',
                    template: '<p class="text-center">Would you remove the selected photos?</p>',
                    cancelType: 'button-assertive'
                  }).then(function (res) {
                    if (res) {
                      $ionicListDelegate.closeOptionButtons();
                      var callsList = [];
                      for (var y = 0; y < $scope.photosToDelete.length; y++) {
                        callsList.push(ImagesService.deleteImages(vm.obj.userId, vm.obj.id, $scope.photosToDelete[y]));
                      }
                      $ionicLoading.show({ templateUrl: 'templates/spinner.html' });
                      $q.all(callsList)
                        .then(function (response) {
                          $rootScope.$broadcast('update-image');
                        }).finally(function () {
                          $ionicLoading.hide();
                          $scope.photosToDelete = {};
                          return true;
                        });
                    }
                  });
                }
              }
              vm.shouldShowDelete = !vm.shouldShowDelete;
              return true;
            case 1:
              var options = {
                quality: 75,
                destinationType: Camera.DestinationType.DATA_URL,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 300,
                targetHeight: 300,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false
              };
              $cordovaCamera.getPicture(options).then(function (imageData) {
                vm.obj.pictureBook = "data:image/jpeg;base64," + imageData;
                ImagesService.createImage(vm.obj.userId, vm.obj.id, vm.obj.id, imageData)
                  .then(function (entity) {
                    $rootScope.$broadcast('update-image');
                  });
              });
              return true;
          }
        }
      });
    }

    function closeModal() {
      $scope.modal.hide();
    };

    function goToSlide(index) {
      if(!vm.shouldShowDelete){
        $scope.modal.show();
        $ionicSlideBoxDelegate.slide(index);
      }
    }

    function slideChanged(index) {
      $scope.slideIndex = index;
    };

    $scope.$on('update-image', function (event, args) {
      getListPhoto();
    });

    $scope.$on('user-loggedOut', function (event, args) {
      vm.userLoggedIn = {};
      LocalService.destroy('user-loggedIn');
    });

    $scope.$on('$ionicView.loaded', function () {

      $log.info("Entity Detail View enter...");

      vm.userLoggedIn = LocalService.get('user-loggedIn');

      vm.obj = $stateParams.obj;

      getListPhoto();

    });


  }
})(angular);