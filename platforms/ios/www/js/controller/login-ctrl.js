(function (angular) {
  'use strict';

  angular.module('starter').controller('LoginCtrl', loginCtrl);

  loginCtrl.$inject = ['$scope', '$ionicLoading', 'UserService', '$log', '$rootScope', 'LocalService'];

  function loginCtrl($scope, $ionicLoading, UserService, $log, $rootScope, LocalService) {

    $scope.userLoggedIn = {};

    $scope.closeLogin = function () {
      $scope.loginData = {};
      $scope.loginCtrl.hide();
    };

    $scope.doLogin = function () {
      $ionicLoading.show({ templateUrl: 'templates/spinner.html' });
      UserService.login($scope.loginData)
        .then(function (user) {
          $scope.userLoggedIn = user;
          LocalService.set('user-loggedIn',$scope.userLoggedIn);
        }).finally(function () {
          $ionicLoading.hide();
          $scope.loginCtrl.hide();
          $rootScope.$broadcast('user-loggedIn');
        });
    };

  }
})(angular);