(function (angular) {
  'use strict';

  angular.module('starter').controller('HomeCtrl', homeCtrl);

  homeCtrl.$inject = ['$scope', '$q', '$log', '$ionicLoading', '$ionicListDelegate',
    '$ionicPopup', '$ionicModal', '$state', '$rootScope',
    'EntitiesService', 'NationalitiesService', 'ImagesService',
    'LocalService'];

  function homeCtrl($scope, $q, $log, $ionicLoading, $ionicListDelegate,
    $ionicPopup, $ionicModal, $state, $rootScope, EntitiesService,
    NationalitiesService, ImagesService, LocalService) {

    var vm = this;

    vm.addEntity = addEntity;
    vm.deleteEntity = deleteEntity;
    vm.modifyEntity = modifyEntity;
    vm.getEntityDetail = getEntityDetail;
    vm.list = [];
    vm.userLoggedIn = {};
    $scope.selectedEntity = {};

    $ionicModal.fromTemplateUrl('templates/entity-add.html', function (modal) {
      $scope.homeCtrl = modal;
    }, {
        scope: $scope,
        animation: 'slide-in-left',//'slide-left-right', 'slide-in-up', 'slide-right-left'
        focusFirstInput: true
      });

    function addEntity() {
      setTimeout(function () {
        $scope.selectedEntity = {};
        $scope.homeCtrl.show();
      }, 10);
    }

    function deleteEntity(entity) {
      $ionicPopup.confirm({
        title: 'Warning',
        template: '<p class="text-center">Would you remove this entity from the list?</p>',
        cancelType: 'button-assertive'
      }).then(function (res) {
        if (res) {
          $ionicListDelegate.closeOptionButtons();
          EntitiesService.deleteEntity(entity)
            .then(function (entity) {
              $rootScope.$broadcast('update-list');
            });
        }
      });
    }

    function modifyEntity(entity) {
      setTimeout(function () {
        $scope.selectedEntity = entity;
        $scope.homeCtrl.show();
      }, 10);
    }

    function getEntityDetail(entity) {
      $state.go('app.detail', { obj: entity });
    }

    function getListEntity() {
      $ionicLoading.show({ templateUrl: 'templates/spinner.html' });
      EntitiesService.listEntity()
        .then(function (entities) {
          var callsList = [];
          for (var i = 0; i < entities.length; i++) {
            if (entities[i].id != null && entities[i].userId != null && entities[i].pictureBook != null) {
              callsList.push(ImagesService.getImage(entities[i].userId, entities[i].id, entities[i].pictureBook));
            }
          }
          $q.all(callsList)
            .then(function (response) {
              for (var i = 0; i < entities.length; i++) {
                for (var z = 0; z < response.length; z++) {
                  if (entities[i].id == response[z].id) {
                    /* Hack */ /* entities[y].pictureBook = response[z].pictureBook; */
                    /* Hack */ entities[i].pictureBook = 'http://lorempixel.com/200/200/people/' + Math.floor(Math.random() * 10);
                    entities[i].filename = response[z].fileName;
                  }
                }
              }
              vm.list = vm.list.concat(entities);
            }).finally(function () {
              $ionicLoading.hide();
            });
        });
    }

    $scope.swipeDown = function () {
      getListEntity();
    }

    $scope.$on('$ionicView.loaded', function (evt, data) {
      $ionicLoading.show({ templateUrl: 'templates/spinner.html' });
      getListEntity();
    });

    $scope.$on('update-list', function (event, args) {
      getListEntity();
    });

    $scope.$on('user-loggedIn', function (event, args) {
      vm.userLoggedIn = LocalService.get('user-loggedIn');
    });

    $scope.$on('user-loggedOut', function (event, args) {
      vm.userLoggedIn = {};
      LocalService.destroy('user-loggedIn');
    });

  }
})(angular);